n = int(input())
i = 0

while i <= n:
    s = 2 ** i
    if s <= n:
        print(s, sep=' ', end=' ')
    i += 1

